<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Table</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Samandar">
    <link rel="stylesheet" href="/webroot/css/foundation.css">
    <link rel="stylesheet" href="/webroot/css/style.css">
</head>

<body>
<h5>MorskoyBoy</h5>
<div id="table11">
    <#assign i=1>
    <#list captainBoard as innerArray>
        <div class="tiny-2 columns">
            <#assign j=1>
            <#list innerArray as itemValue>
                <#if i==11>
                    <a href="/" class="radius small alert button">X</a>
                </#if>
                <#if j==11>
                    <a href="/" class="radius small alert button">X</a>
                </#if>
                <#if i!=11&&j!=11>
                    <a href="/welcome/${player}/${j}/${i}" class="radius small secondary button">${itemValue}</a>
                </#if>
                <#assign j=j+1>
            </#list>
        </div>
        <#assign i=i+1>
    </#list>
</div>



<br>



<div id="table 2">
    <#if counterOfShips!=0>
        <div id="numberOfAvailableShipsToBuild">
            <#assign ships=counterOfShips>
            <h6 style="text-align: center;">You can build: ${ships} ships</h6>
        </div>
    </#if>
    <#if counterOfShips==0>
        <a href="/welcome/${player}/play/${counterOfShips}/0/0" style="text-align: center;" class="radius medium success button">Play</a>
    </#if>
</div>
<br>
<div id="isCreated">
    <#if isCreated==true>
        <h6 style="background-color: lawngreen;text-align: center;">Created</h6>
    </#if>
    <#if isCreated==false>
        <h6 style="background-color: red;text-align: center;">Failed</h6>
    </#if>
</div>
<br>
<div id="forSize" style="text-align: center;">
    <h5  style="text-align: center;color: white;background-color: black;">Size</h5>
    <#assign size=1>
    <a href="/welcome/${player}/${size}" class="radius medium secondary button">1</a>
    <#assign size=2>
    <a href="/welcome/${player}/${size}" class="radius medium secondary button">2</a>
    <#assign size=3>
    <a href="/welcome/${player}/${size}" class="radius medium secondary button">3</a>
    <#assign size=4>
    <a href="/welcome/${player}/${size}" class="radius medium secondary button">4</a>
</div>
<br>

<div id="forDirection" style="text-align: center;">
    <h6 style="text-align: center;color: white;background-color: black;">Direction</h6>
    <#list 1..3 as j>
        <#list 1..3 as i>
            <#if i==1&&j==2>
                <#assign direction="W">
                <a href="/welcome/${player}/${size}/${direction}/0" class="radius medium secondary button">west</a>
            </#if>
            <#if i==2&&j==1>
                <#assign direction="N">
                <a href="/welcome/${player}/${size}/${direction}/0" class="radius medium secondary button">north</a>
            </#if>
            <#if i==2&&j==3>
                <#assign direction="S">
                <a href="/welcome/${player}/${size}/${direction}/0" class="radius medium secondary button">south</a>
            </#if>
            <#if i==3&&j==2>
                <#assign direction="E">
                <a href="/welcome/${player}/${size}/${direction}/0" class="radius medium secondary button">east</a>
            </#if>
            <a href="#"></a>
        </#list>
        <br>
    </#list>
</div>

</body>
</html>