package uz.samandar.controller

import com.fasterxml.jackson.annotation.JsonAlias
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.templ.freemarker.FreeMarkerTemplateEngine
import uz.samandar.*

class BoardController(vertx: Vertx) {

    val engine=FreeMarkerTemplateEngine.create(vertx)

//---player1 equipment
    var player1:Int=0
    val captainBoard1:JsonArray=makeBoard()//for player1
    val opponentBoard1=makeBoard()//for player1
    var board1= Board()
    var x1:Int=-1
    var y1:Int=-1
    var direction1="NULL"
    var size1:Int=0
    var statusOfLaunch1:Boolean=false
    lateinit var cell1:Cell
    var dataToCreate1 = JsonObject()
    var counterOfShips1=10
    var x1ToFire=-1
    var y1ToFire=-1
    var dataToPlay1 = JsonObject()
    var isCreated1:Boolean=false
    var counterOfDeadShipsPlayer1=0
    var fireResult1:String="NULL"
    var playersReadiness1=0
//---player2 equipment
    var player2:Int=0
    val captainBoard2:JsonArray=makeBoard()
    val opponentBoard2=makeBoard()
    var board2=Board()
    var x2:Int=-1
    var y2:Int=-1
    var direction2="NULL"
    var size2:Int=0
    var statusOfLaunch2:Boolean=false
    lateinit var cell2:Cell
    var dataToCreate2 = JsonObject()
    var counterOfShips2=10
    var x2ToFire=-1
    var y2ToFire=-1
    var dataToPlay2 = JsonObject()
    var isCreated2:Boolean=false
    var counterOfDeadShipsPlayer2=0
    var fireResult2:String="NULL"
    var playersReadiness2=0

    //global
    var winner=0
    var firstTurn=0
    var fireTurn=0
    var dataToWelcome=JsonObject()

    fun welcomeHandler(ctx: RoutingContext){
        dataToWelcome.put("isPlayer1",player1)
        dataToWelcome.put("isPlayer2",player2)
        engine.render(dataToWelcome,"templates/welcome.ftl"){
            if (it.succeeded()) {
                ctx.response().end(it.result())
            } else {
                ctx.response().end(it.cause().message)
            }
        }
    }

//-------------------------------------
fun playHandler(ctx: RoutingContext){
    if ((playersReadiness1+playersReadiness2)==2){
        firstTurn=ctx.request().params().get("player").toInt()
        if(fireTurn==0){
            fireTurn=firstTurn
        }
        if(firstTurn==1){
            player1=1
            dataToPlay1.put("player",player1)
            play1Handler(ctx)
        }else if(firstTurn==2){
            player2=2
            dataToPlay2.put("player",player2)
            play2Handler(ctx)
        }
    }else{
            if(playersReadiness1==1){
                dataToWelcome.put("player",player1)
            }else if(playersReadiness2==1){
                dataToWelcome.put("player",player2)
            }
            engine.render(dataToWelcome,"templates/wait.ftl"){
                if (it.succeeded()) {
                    ctx.response().end(it.result())
                } else {
                    ctx.response().end(it.cause().message)
                }
            }

    }
}

    fun play1Handler(ctx: RoutingContext){
        if(counterOfDeadShipsPlayer1!=10&&counterOfDeadShipsPlayer2!=10){
                dataToPlay1.put("captainBoard", captainBoard1)
                dataToPlay1.put("fireTurn", fireTurn)
                dataToPlay1.put("opponentBoard", opponentBoard1)
                dataToPlay1.put("fireResult", fireResult1)
                dataToPlay1.put("counterOfDeadShips", counterOfDeadShipsPlayer1)
                engine.render(dataToPlay1, "templates/play.ftl") {
                    if (it.succeeded()) {
                        ctx.response().end(it.result())
                    } else {
                        ctx.response().end(it.cause().message)
                    }
                }
        }else{
            if(counterOfDeadShipsPlayer2==10){
                winner=player1
            }else if(counterOfDeadShipsPlayer1==10){
                winner=player2
            }
            dataToPlay1.put("winner",winner)
            engine.render(dataToPlay1,"templates/gameResult.ftl"){
                if (it.succeeded()) {
                    ctx.response().end(it.result())
                } else {
                    ctx.response().end(it.cause().message)
                }
            }
        }
    }

    fun play2Handler(ctx: RoutingContext){
        if(counterOfDeadShipsPlayer2!=10&&counterOfDeadShipsPlayer2!=10){
                dataToPlay2.put("captainBoard", captainBoard2)
                dataToPlay2.put("fireTurn", fireTurn)
                dataToPlay2.put("opponentBoard", opponentBoard2)
                dataToPlay2.put("fireResult", fireResult2)
                dataToPlay2.put("counterOfDeadShips", counterOfDeadShipsPlayer2)
                engine.render(dataToPlay2, "templates/play.ftl") {
                    if (it.succeeded()) {
                        ctx.response().end(it.result())
                    } else {
                        ctx.response().end(it.cause().message)
                    }
                }
        }else{
            if(counterOfDeadShipsPlayer2==10){
                winner=player1
            }else if(counterOfDeadShipsPlayer1==10){
                winner=player2
            }
            dataToPlay2.put("winner",winner)
            engine.render(dataToPlay2,"templates/gameResult.ftl"){
                if (it.succeeded()) {
                    ctx.response().end(it.result())
                } else {
                    ctx.response().end(it.cause().message)
                }
            }
        }
    }


    fun fireHandler(ctx: RoutingContext){
        firstTurn=ctx.request().params().get("player").toInt()
        if(firstTurn==1){
            if(fireTurn==1){
                fire1Handler(ctx)
            }else{
                play1Handler(ctx)
            }
        }else if(firstTurn==2){
            if(fireTurn==2){
                fire2Handler(ctx)
            }else{
                play2Handler(ctx)
            }
        }
    }
    fun fire1Handler(ctx: RoutingContext){
        x1ToFire=ctx.request().params().get("x").toInt()
        y1ToFire=ctx.request().params().get("y").toInt()
        cell1=Cell(x1ToFire,y1ToFire)
        fireResult1=board2.fire(cell1)
        println("fire result -> $fireResult1")
        x1ToFire--
        y1ToFire--
        if(fireResult1== SHIP_STATUS_FIRED){
        opponentBoard1.getJsonArray(y1ToFire).set(x1ToFire,"F")
        }else if(fireResult1== SHIP_STATUS_DIED){
            opponentBoard1.getJsonArray(y1ToFire).set(x1ToFire,"F")
            counterOfDeadShipsPlayer1++;
        }else if(fireResult1== SHIP_STATUS_MISSED){
            opponentBoard1.getJsonArray(y1ToFire).set(x1ToFire,"M")
            fireTurn=2
        }
        dataToPlay1.put("fireResult",fireResult1)
        play1Handler(ctx)
    }

    fun fire2Handler(ctx: RoutingContext){
        x2ToFire=ctx.request().params().get("x").toInt()
        y2ToFire=ctx.request().params().get("y").toInt()
        cell2=Cell(x2ToFire,y2ToFire)
        fireResult2=board1.fire(cell2)
        println("fire result -> $fireResult2")
        x2ToFire--
        y2ToFire--
        if(fireResult2== SHIP_STATUS_FIRED){
            opponentBoard2.getJsonArray(y2ToFire).set(x2ToFire,"F")
        }else if(fireResult2== SHIP_STATUS_DIED){
            opponentBoard2.getJsonArray(y2ToFire).set(x2ToFire,"F")
            counterOfDeadShipsPlayer2++;
        }else if(fireResult2== SHIP_STATUS_MISSED){
            opponentBoard2.getJsonArray(y2ToFire).set(x2ToFire,"M")
            fireTurn=1
        }
        dataToPlay2.put("fireResult",fireResult2)
        play2Handler(ctx)
    }


//---------------------------------------------------------------------------------------------------------------------------

    fun indexHandler(ctx: RoutingContext){
        firstTurn=ctx.request().params().get("player").toInt()
        if(firstTurn==1){
            player1=1
            dataToCreate1.put("player",player1)
            index1Handler(ctx)
        }else if(firstTurn==2){
            player2=2
            dataToCreate2.put("player",player2)
            index2Handler(ctx)
        }
    }

    fun index1Handler(ctx: RoutingContext) {
        dataToCreate1.put("captainBoard",captainBoard1)
        dataToCreate1.put("counterOfShips",counterOfShips1)
        dataToCreate1.put("isCreated",isCreated1)
        dataToCreate1.put("x",x1)
        dataToCreate1.put("y",y1)
        dataToCreate1.put("size",size1)
        dataToCreate1.put("direction",direction1)
        println("$x1,$y1,$size1,$direction1")
        println(statusOfLaunch1)
        engine.render(dataToCreate1, "templates/board.ftl") {
                if (it.succeeded()) {
                    ctx.response().end(it.result())
                } else {
                    ctx.response().end(it.cause().message)
                }
            }//генерация кода HTML
    }

    fun index2Handler(ctx: RoutingContext) {
        dataToCreate2.put("captainBoard",captainBoard2)
        dataToCreate2.put("counterOfShips",counterOfShips2)
        dataToCreate2.put("isCreated",isCreated2)
        dataToCreate2.put("x",x2)
        dataToCreate2.put("y",y2)
        dataToCreate2.put("size",size2)
        dataToCreate2.put("direction",direction2)
        println("$x2,$y2,$size2,$direction2")
        println(statusOfLaunch2)
        engine.render(dataToCreate2, "templates/board.ftl") {
            if (it.succeeded()) {
                ctx.response().end(it.result())
            } else {
                ctx.response().end(it.cause().message)
            }
        }//генерация кода HTML
    }
    fun directionHandler(ctx: RoutingContext){
        firstTurn=ctx.request().params().get("player").toInt()
        if(firstTurn==1){
            direction1Handler(ctx)
        }else if(firstTurn==2){
            direction2Handler(ctx)
        }
    }

    fun direction1Handler(ctx: RoutingContext){
        direction1=ctx.request().params().get("direction").toString()
        isCreated1=createShip1()
        dataToCreate1.put("isCreated",isCreated1)
        if(isCreated1){
            counterOfShips1--
            if(counterOfShips1==0){
                playersReadiness1++;
            }
            x1--
            y1--
            for(l in 0..(size1-1)) {
                if (direction1 == SHIP_DIRECTION_EAST) {
                    captainBoard1.getJsonArray(y1).set(x1 + l, "B")
                } else if (direction1 == SHIP_DIRECTION_WEST) {
                    captainBoard1.getJsonArray(y1).set(x1 - l, "B")
                } else if (direction1 == SHIP_DIRECTION_NORTH) {
                    captainBoard1.getJsonArray(y1-l).set(x1, "B")
                } else if (direction1 == SHIP_DIRECTION_SOUTH) {
                    captainBoard1.getJsonArray(y1+l).set(x1, "B")
                }
            }
        }
        index1Handler(ctx)

    }

    fun direction2Handler(ctx: RoutingContext){
        direction2=ctx.request().params().get("direction").toString()
        isCreated2=createShip2()
        dataToCreate2.put("isCreated",isCreated2)
        if(isCreated2){
            counterOfShips2--
            if(counterOfShips2==0){
                playersReadiness2++;
            }
            x2--
            y2--
            for(l in 0..(size2-1)) {
                if (direction2 == SHIP_DIRECTION_EAST) {
                    captainBoard2.getJsonArray(y2).set(x2 + l, "B")
                } else if (direction2 == SHIP_DIRECTION_WEST) {
                    captainBoard2.getJsonArray(y2).set(x2 - l, "B")
                } else if (direction2 == SHIP_DIRECTION_NORTH) {
                    captainBoard2.getJsonArray(y2-l).set(x2, "B")
                } else if (direction2 == SHIP_DIRECTION_SOUTH) {
                    captainBoard2.getJsonArray(y2+l).set(x2, "B")
                }
            }
        }
        index2Handler(ctx)
    }



    fun sizeHandler(ctx: RoutingContext){
        firstTurn=ctx.request().params().get("player").toInt()
        if(firstTurn==1){
            size1Handler(ctx)
        }else if(firstTurn==2){
            size2Handler(ctx)
        }
    }
    fun size1Handler(ctx: RoutingContext){
        size1=ctx.request().params().get("size").toInt()
        index1Handler(ctx);
    }
    fun size2Handler(ctx: RoutingContext){
        size2=ctx.request().params().get("size").toInt()
        index2Handler(ctx);
    }



    fun createShip1():Boolean{
        if(x1!=-1&&x1!=11&&y1!=-1&&y1!=11&&size1!=0){
            cell1=Cell(x1,y1)
                statusOfLaunch1=board1.addShips(size1,cell1,direction1)

            println(statusOfLaunch1)
        }
        return statusOfLaunch1
    }

    fun createShip2():Boolean{
        if(x2!=-1&&x2!=11&&y2!=-1&&y2!=11&&size2!=0){
            cell2=Cell(x2,y2)
            statusOfLaunch2=board2.addShips(size2,cell2,direction2)

            println(statusOfLaunch2)
        }
        return statusOfLaunch2
    }


    fun coordinateHandler(ctx:RoutingContext){
        firstTurn=ctx.request().params().get("player").toInt()
        if(firstTurn==1){
            coordinate1Handler(ctx)
        }else if(firstTurn==2){
            coordinate2Handler(ctx)
        }
    }

    fun coordinate1Handler(ctx:RoutingContext){
        x1=ctx.request().params().get("x").toInt()
        y1=ctx.request().params().get("y").toInt()
        index1Handler(ctx)
    }

    fun coordinate2Handler(ctx:RoutingContext){
        x2=ctx.request().params().get("x").toInt()
        y2=ctx.request().params().get("y").toInt()
        index2Handler(ctx)
    }

    fun makeBoard():JsonArray{
        var tableArray = JsonArray()
        for (k in 1..11){
            var innerArray = JsonArray()
            for (h in 1..11) {
                innerArray.add("-")
            }
            tableArray.add(innerArray)
        }
        return tableArray
    }


}









