%include "io64.inc"
section .data 
G db "Hello Sir!",0
name db "What is your name?",0
section .text
global CMAIN
CMAIN:
    mov rbp, rsp; for correct debugging
    ;write your code here
    xor rax, rax
    PRINT_STRING G
    NEWLINE 
    PRINT_STRING name
    ret