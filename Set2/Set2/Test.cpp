#include "Test.h"
#include <iostream>
#include <time.h>
#include <set>
using namespace std;

void Test::print_content()
{
	this->set_to_test->print_content();
}

void Test::insert_find()
{
	srand(time(NULL));
	int result; 
	bool error=false;
	int* arr = new int[1000];
	for (int i = 0; i < 1000; i++) {
		arr[i] = rand() % 1000 + 1;
		result = this->set_to_test->insert(&arr[i], sizeof(arr[i]));
		if (result==-2) {
			error = true;
			cout << "INSERT TEST -> failed to insert " << arr[i] << " at position #" << i << endl;
			break;
		}
	}
	if (error == false) {
		cout << "INSERT TEST -> success !" << endl;
	}
	int minimum = arr[0];
	int minimum_index = 0;
	size_t size;
	for (int i = 1; i < 1000; i++) {
		if (arr[i] < minimum) {
			minimum = arr[i];
			minimum_index = i;
		}
	}
	Set::SetIterator* iter = this->set_to_test->find(&arr[minimum_index], sizeof(arr[minimum_index]));
	if (iter != NULL) {
		if (memcmp(iter->getElement(size), &arr[minimum_index], sizeof(arr[minimum_index])) == 0) {
			cout << "FOUND -> success !" << endl;
		}
	}
	else {
		cout << " FOUND  -> fail ! " << endl;
	}
}

void Test::iterator()
{
	Set::SetIterator* iter = this->set_to_test->newIterator();
	size_t size = 0;
	int i = 0;
	Set::SetIterator* iter_cmp = this->set_to_test->end();
	for (iter = this->set_to_test->begin(); iter->equals(iter_cmp)!=false; iter->goToNext()) {
		i++;
	}
	if (i != this->set_to_test->size()) {
		cout << "ITERATOR TEST -> fail !" << endl;
	}
	else {
		cout << "ITERATOR TEST -> success !" << endl;
	}
}

void Test::remove()
{
	Set::SetIterator* iter = this->set_to_test->begin();
	int size_of_set = this->set_to_test->size();
	this->set_to_test->remove(iter);	
	if (size_of_set>this->set_to_test->size()) {
		cout << "DELETE -> success !" << endl;
	}
	else {
		cout << "DELETE -> fail !" << endl;
	}
}

void Test::clear()
{
	this->set_to_test->clear();
	if (this->set_to_test->size() == 0) {
		cout << "CLEAR -> success ! " << endl;
	}
	else {
		cout << "CLEAR -> fail ! " << endl;
	}
}

void Test::speed()
{
	set<int> numbers;
	srand(time(NULL));
	int* arr = new int[1000];
	for (int i = 0; i < 1000; i++) {
		arr[i] = rand() % 1000 + 1;
	}

	cout << " My set speed : " << endl;
	int s = clock();
	cout << "time begin : " << s << endl;
	for (int i = 0; i < 1000; i++) {
		this->set_to_test->insert(&arr[i], sizeof(arr[i]));
	}
	s = clock();
	cout << "time end : " << s << endl;
	cout << "endl" << endl;
	cout << "STL set speed : " << endl;
	s = clock();
	cout << "time begin : " << s << endl;
	for (int i = 0; i < 1000; i++) {
		numbers.insert(arr[i]);
	}
	s = clock();
	cout << "time end : " << s << endl;
}


