#include <iostream>
#include "Mem.h"
#include "Set.h"
#include "Test.h"
#include <time.h>
using namespace std;

//escalate tests:
//- write 3 types of insert test : easy,medium, hard
//- test speed for insert remove and find

int main() {

	try{
		Mem memory(100000);
		Set a(memory);
		Test test(&a);
		test.insert_find();
		test.iterator();		
		test.remove();
		test.clear();
		test.remove();
		test.speed();
		
	}
	catch (Container::Error err) {
		cout <<"\"EXCEPTION MESSAGE\" : "<< err.msg << endl;
	}
	return 0;
}