﻿
#pragma once
#include "Container.h"
class GroupContainer : public Container
{
public:

	GroupContainer(MemoryManager& mem) : Container(mem) {}

	size_t max_bytes() {
		return _memory.maxBytes();
	}

};
