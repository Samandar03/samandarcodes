﻿#pragma once
#include "MemoryManager.h"
#include <iostream>

using namespace std;

//added_memory_value is used to keep in mind how much ewual data we need to add/subtract every time


// Простейший менеджер памяти, использует ::new и ::delete
class Mem : public MemoryManager
{
private:
	size_t memory_capacity;
	size_t added_memory_value;
public:
	Mem(size_t sz) : MemoryManager(sz) {
		this->memory_capacity = sz;
		this->added_memory_value = 0;

	}

	void* allocMem(size_t sz) {
		if (this->added_memory_value == 0) {
			this->added_memory_value = sz;
		}
		this->memory_capacity -= sz;
		return new char[sz];
	}


	void freeMem(void* ptr) {
		this->memory_capacity += this->added_memory_value;
		delete[] ptr;
	}


	size_t maxBytes() {
		return this->memory_capacity;
	}

	size_t capacity() {
		return this->memory_capacity;
	}
};