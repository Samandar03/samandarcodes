#pragma once
#include "MemoryManager.h"
#include "TreeAbstract.h"

class BinaryTree:public AbstractTree {
public:
	class Node {
	public:
		Node* left;
		Node* right;
		Node* parent;
		void* key;
		size_t size;
		Node();
		Node(void* elem, size_t size);
		Node* define_successor();
		Node* minimum_value();
		Node* maximum_value();
		~Node();
	};
	Node* root;
	Node* null_node;
	int cmp_by_memory(void* key1, void* key2, size_t size);
private:
	MemoryManager& memory;
	int number_of_elements;
	void inorder_tree_walk(Node* node);
public:
	BinaryTree(MemoryManager& mem) : AbstractTree(mem),memory(mem){
		this->root = NULL;
		this->null_node = new Node();
		this->number_of_elements = 0;
	}

	int insert(AbstractTree::Iterator* iter,int child_index,void* elem, size_t size); // 0->success; -1->already exists; -2->other
	void erase(void* elem, size_t size);
	void print();
	void append_null_leaf(BinaryTree::Node* node);
	Node* search(Node* node, void* data, size_t size);
	int get_size() { return number_of_elements; }
	size_t get_size_bytes() { return sizeof(Node) * number_of_elements; }	

#pragma region not realized functions
	class Iterator :public AbstractTree::Iterator {
	public :
		bool goToParent() { return false; }
		bool goToChild(int child_index) { return false; }
	};
	int size() { return 0; }
	Iterator* find(void* elem, size_t size) { return nullptr; }
	Iterator* newIterator() { return nullptr; }
	Iterator* begin() { return nullptr; }
	Iterator* end() { return nullptr; }
	void Container::remove(Iterator* iter){}
	void clear(){}
	bool empty() { return false; }
	bool AbstractTree::remove(Iterator* iter, int leaf_only) { return false; }

#pragma endregion

	~BinaryTree() {
		delete this->root;
		delete this->null_node;
	}
};