#pragma once
#include "SetAbstract.h"
#include "BinaryTree.h"

class Set :public AbstractSet {
private:
	BinaryTree* tree;
public:
	Set(MemoryManager& mem) :AbstractSet(mem) {
		this->tree = new BinaryTree(_memory);
	}

	int insert(void* elem, size_t size);
	void print_content();

	class SetIterator : public AbstractSet::Iterator {
	private:
		BinaryTree::Node* current;
	public:
		SetIterator(BinaryTree::Node* node) { this->current = node; }
		SetIterator() { this->current = NULL; }
		void* getElement(size_t& size);
		bool hasNext();
		void goToNext();
		bool equals(Iterator* right);
		~SetIterator() { delete current; }
	};

	int size();
	SetIterator* find(void* elem, size_t size);
	SetIterator* newIterator();
	SetIterator* begin();
	SetIterator* end();
	void remove(Iterator* iter);
	void clear();
	bool empty();

	~Set() { delete tree; }
};






