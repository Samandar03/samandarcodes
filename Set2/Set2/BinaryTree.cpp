#include "BinaryTree.h"
#include <iostream>

using namespace std;


BinaryTree::Node::~Node() {
	this->left = NULL;
	this->right = NULL;
	this->parent = NULL;
	this->key = NULL;
}

BinaryTree::Node::Node() {
	this->left = NULL;
	this->right = NULL;
	this->parent = NULL;
	this->key = NULL;
	this->size = 0;
}

BinaryTree::Node::Node(void* data, size_t size) {
	this->left = NULL;
	this->right = NULL;
	this->parent = NULL;
	this->key = data;
	this->size = size;
}

int BinaryTree::cmp_by_memory(void* key1, void* key2, size_t size)
{

	int temp_size = (int)size;
	char* var1, * var2;
	var1 = new char[temp_size];
	var2 = new char[temp_size];
	for (int i = 0; i < temp_size; i++) {
		var1[i] = ((char*)key1)[temp_size - i - 1];
		var2[i] = ((char*)key2)[temp_size - i - 1];
	}
	if (memcmp(var1, var2, size) < 0) {
		return -1;
	}
	else if (memcmp(var1, var2, size) > 0) {
		return 1;
	}
	return 0;
}

void BinaryTree::append_null_leaf(BinaryTree::Node* node) {
	node->right = this->null_node;
}
// memory manager addition : firstly put in anywhere, if succedssfully done then use the placement new to change the addres
int BinaryTree::insert(AbstractTree::Iterator* iter , int child_index , void* data , size_t size ) {
	Node* temp1, * temp2, * node_to_insert;
	node_to_insert = new Node(data, size);
	if (node_to_insert != NULL) {
		temp1 = NULL;
		temp2 = this->root;
		while (temp2 != NULL && temp2 != this->null_node) {
			temp1 = temp2;
			if (size == temp2->size) {
				if (cmp_by_memory(node_to_insert->key, temp2->key, node_to_insert->size) < 0) {
					temp2 = temp2->left;
				}
				else if (cmp_by_memory(node_to_insert->key, temp2->key, node_to_insert->size) > 0) {
					temp2 = temp2->right;
				}
				else {
					return -1;
				}
			}
			else {
				return -2;
			}
		}
		void* addr = memory.allocMem(sizeof(Node));
		node_to_insert = new (addr) Node(data, size);
		if (node_to_insert != NULL) {
			node_to_insert->parent = temp1;
		}
		if (temp1 == NULL) {
			this->root = node_to_insert;
			append_null_leaf(this->root);
		}
		else if (cmp_by_memory(node_to_insert->key, temp1->key, node_to_insert->size) < 0) {
			temp1->left = node_to_insert;
		}
		else {
			temp1->right = node_to_insert;
			if (this->root->maximum_value() == temp1->right) {
				append_null_leaf(node_to_insert);
			}
		}
		number_of_elements++;
		return 0;
	}
	return -2;
}



void BinaryTree::print() {
	BinaryTree::inorder_tree_walk(this->root);
}

void BinaryTree::inorder_tree_walk(Node* node) {
	if (node != NULL) {
		BinaryTree::inorder_tree_walk(node->left);
		cout << node->key << " || ";
		BinaryTree::inorder_tree_walk(node->right);
	}
}

void BinaryTree::erase(void* data, size_t size) {
	Node* node_to_delete = search(this->root, data, size);
	Node* temp_node1 = NULL;
	Node* temp_node2 = NULL;
	if (node_to_delete->left == NULL || node_to_delete->right == NULL) {
		temp_node1 = node_to_delete;
	}
	else {
		temp_node1 = node_to_delete->define_successor();
	}

	if (temp_node1->left != NULL) {
		temp_node2 = temp_node1->left;
	}
	else {
		temp_node2 = temp_node1->right;
	}

	if (temp_node2 != NULL) {
		temp_node2->parent = temp_node1->parent;
	}

	if (temp_node1->parent == NULL) {
		this->root = temp_node2;
	}
	else if (temp_node1 == temp_node1->parent->left) {
		(temp_node1->parent)->left = temp_node2;
	}
	else {
		(temp_node1->parent)->right = temp_node2;
	}

	if (temp_node1 != node_to_delete) {
		node_to_delete->key = temp_node1->key;
		node_to_delete->size = temp_node1->size;
	}
	number_of_elements--;
	memory.freeMem(node_to_delete);
}
// NODE* NODE = THIS is used to solve the problem with anachronism
//successor defines the least element which is next to given node in the tree
BinaryTree::Node* BinaryTree::Node::define_successor() {
	Node* node = this;
	if (node->right != NULL) {
		return node->right->minimum_value();
	}
	BinaryTree::Node* def_parent = node->parent;
	while (def_parent != NULL && node == def_parent->right) {
		node = def_parent;
		def_parent = def_parent->parent;
	}
	return def_parent;
}



BinaryTree::Node* BinaryTree::Node::minimum_value()
{
	Node* temp = this;
	while (temp->left != NULL) {
		temp = temp->left;
	}
	return temp;
}

BinaryTree::Node* BinaryTree::Node::maximum_value()
{
	Node* node = this;
	while (node->right != NULL)
	{
		node = node->right;
	}
	return node;
}


BinaryTree::Node* BinaryTree::search(Node* node, void* key, size_t size) {
	if (key != NULL && node != NULL) {
		if (node == NULL || cmp_by_memory(node->key, key, size) == 0) {
			return node;
		}
		if (cmp_by_memory(key, node->key, size) < 0) {
			return BinaryTree::search(node->left, key, size);
		}
		else {
			return BinaryTree::search(node->right, key, size);
		}
	}
	return NULL;
}


