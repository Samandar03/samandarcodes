#include <iostream>	
#include "Set.h"

using namespace std;



#pragma region ForSet

int Set::insert(void* elem, size_t size)
{
	if (max_bytes() == 0) throw Error("Memory is not enough to insert !");
	else {
		return tree->insert(nullptr,0,elem, size);
	}
}

void Set::print_content() {
	if (empty()) throw Error("cannot print content, tree is empty ! ");
	else {
		tree->print();
	}
}



int Set::size()
{
	return this->tree->get_size();
}



Set::SetIterator* Set::find(void* elem, size_t size)
{
	BinaryTree::Node* item_to_search =tree->search(this->tree->root, elem, size);
	if (item_to_search != NULL) {
		SetIterator* iter_temp = new SetIterator(item_to_search);
		return iter_temp;
	}
	return nullptr;
}

Set::SetIterator* Set::newIterator()
{
	SetIterator* new_iter = new  SetIterator();
	return new_iter;
}

Set::SetIterator* Set::begin()
{
	if (!empty()) {
		SetIterator* tmp_iter = new SetIterator(this->tree->root->minimum_value());
		return tmp_iter;
	}
	return nullptr;
}

Set::SetIterator* Set::end()
{
	if (!empty()) {
		SetIterator* iter_temp = new SetIterator(this->tree->root->maximum_value());
		return iter_temp;
	}
	return nullptr;
}


void Set::remove(Iterator* iter) {
	if (this->empty()) throw Error("tree is empty cannot remove! ");
	else {
		size_t size = 0;
		void* addr = iter->getElement(size);
		iter->goToNext();
		this->tree->erase(addr, size);
	}
}

void Set::clear()
{
	int size_of_set = this->size();
	Set::SetIterator* iter = this->begin();
	for (int i = 0; i < size_of_set; i++) {
		this->remove(iter);
	}
	if (this->size() == 0) {
		if (this->tree->root == this->tree->null_node) {
			this->tree->root = NULL;
		}
	}
}

bool Set::empty()
{
	if (size() == 0) {
		return true;
	}
	return false;
}



#pragma endregion

//-------------------------------------------------------------------------------------------------------------

#pragma region forSetIterator

void* Set::SetIterator::getElement(size_t& size)
{
	size = this->current->size;
	return this->current->key;
}

bool Set::SetIterator::hasNext()
{
	BinaryTree::Node* temp = (BinaryTree::Node*)this->current->define_successor();
	if (temp != NULL) {
		if (temp->right == (BinaryTree::Node*)this->current) {
			return false;
		}
		else {
			return true;
		}
	}
	return false;
}

void Set::SetIterator::goToNext()
{
	if (hasNext()) {
		this->current = this->current->define_successor();
	}
	else {
		cout << "No  member" << endl;
	}
}

bool Set::SetIterator::equals(Iterator* right)
{
	size_t size = 0;
	void* address = right->getElement(size);
	if (address != NULL) {
		if (memcmp(this->current->key,address, this->current->size) == 0) {
			return true;
		}
	}
	else {
		if (this->current->key) {
			return true;
		}
	}

	return false;
}


#pragma endregion






