#pragma once
#include "Set.h"

class Test {
private:
	Set* set_to_test;
public:
	Test(Set* a) {
		this->set_to_test = a;
	}

	void print_content();
	void insert_find();
	void iterator();
	void remove();
	void clear();
	void speed();
};